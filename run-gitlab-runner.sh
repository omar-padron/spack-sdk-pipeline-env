#! /usr/bin/env bash

[ "$0" '=' "$BASH_SOURCE" ]
sourced="$?"

if [ "$sourced" '=' '0' ] ; then
    set -eo pipefail
    trap "exit \$SYSTEM_FAILURE_EXIT_CODE" ERR
fi

usage() {
    echo "$0 ACTION [...ARGS]"
    exit 1
}

sha164() {
    sha1sum |
        sed 's/^\(.\{32\}\)\([^ ]\+\).*/00000000: \1\n00000010: \2/g' |
        xxd -r |
        base64 |
        tr -d '=' |
        tr '/' '-'
}

sha1_hex() {
    sha1sum | sed 's/^\(.\{40\}\).*/\1/g'
}

local_script="$BASH_SOURCE"
if [ -z "$local_script" ] ; then local_script="$0" ; fi

local_script_dir="$( dirname "$local_script" )"

script="$( readlink -e "$local_script" )"
script_dir="$( readlink -e "$( dirname "$script" )" )"

load_user_config() {
    if [ '!' -f  "$script_dir/config" ] ; then
        if [ -f '/cache/global/config' ] ; then
            echo 'Inside container: skipping user config'
            return
        else
            echo "User configuration not found: $script_dir/config" >&2
            exit 1
        fi
    fi

    . "$script_dir/config"
    cache_dir="$script_dir/cache"
    local_cache_dir="$cache_dir/${namespace}"
    global_cache_dir="$cache_dir/_global"
    config_file="$local_cache_dir/config"

    if [ -z "$data_prefix" ] ; then
        data_prefix="$local_cache_dir/data"
    fi

    if [ -z "$scratch_prefix" ] ; then
        scratch_prefix="$local_cache_dir/data"
    fi

    data_dir="$data_prefix/$namespace"
    scratch_dir="$scratch_prefix/$namespace"
}

unregister() {
    "$glr" unregister --config "$config_toml" --name 'rigel'
}

load_config() {
    load_user_config

    if [ -f "$config_file" ] ; then
        . "$config_file"
    elif [ -f '/cache/global/config' ] ; then
        echo 'Inside container: loading prepared config.'
        . '/cache/global/config'
    else
        (
            echo "No configuration for namespace ${namespace} found."
            echo ""
            echo "Edit the user config file:"
            echo "    $local_script_dir/config"
            echo ""
            echo "and run:"
            echo "    $local_script configure"
            echo ""
            echo "to generate the configuration."
        ) >&2
        exit 1
    fi

    network="$namespace"
    minio_container_name="${namespace}-minio"
    git_mirror_container_name="${namespace}-git-mirror"
    builder_container_name="${namespace}-builder"
    util_container_name="${namespace}-util"

    base_image_tag="$( echo "$base_image" | tr -d ':' )"
    builder_tag="${namespace}-builder:$base_image_tag"
    util_tag="${namespace}-util:$base_image_tag"

    glr="$global_cache_dir/gitlab-runner"
    config_toml="$local_cache_dir/config.toml"
    glci="$local_cache_dir/ci"
    ci_builds="$glci/builds"
    ci_cache="$glci/cache"

    runner_slug="$(
        (
            echo -n "${CUSTOM_ENV_CI_RUNNER_ID}"
        ) | sha1_hex )"

    project_slug="$(
        (
            echo -n "${CUSTOM_ENV_CI_PROJECT_ID}"
            echo -n "${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}"
            echo -n "${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}"
        ) | sha1_hex )"

    pipeline_slug="$(
        (
            echo -n "${CUSTOM_ENV_CI_PROJECT_ID}"
            echo -n "${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}"
            echo -n "${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}"
            echo -n "${CUSTOM_ENV_CI_PIPELINE_ID}"
        ) | sha1_hex )"

    job_slug="$(
        (
            echo -n "${CUSTOM_ENV_CI_RUNNER_ID}"
            echo -n "${CUSTOM_ENV_CI_PROJECT_ID}"
            echo -n "${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}"
            echo -n "${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}"
            echo -n "${CUSTOM_ENV_CI_PIPELINE_ID}"
            echo -n "${CUSTOM_ENV_CI_JOB_ID}"
        ) | sha1_hex )"
}

write_config() {
    cache_dir="$script_dir/cache"
    local_cache_dir="$cache_dir/${namespace}"
    global_cache_dir="$cache_dir/_global"

    mkdir -p "$cache_dir" \
             "$local_cache_dir" \
             "$global_cache_dir" \
             "$data_dir" \
             "$scratch_dir"

    (
        echo "namespace=\"$namespace\""
        echo "base_image=\"$base_image\""
        echo "gitlab_host=\"$gitlab_host\""
        echo "gitlab_project_id=\"$gitlab_project_id\""
        echo "runner_tag_list=\"$runner_tag_list\""
        echo "runner_registration_token=\"$runner_registration_token\""
        echo "upstream_spack_repo=\"$upstream_spack_repo\""
        echo "trigger_token=\"$trigger_token\""
        echo "trigger_ref=\"$trigger_ref\""
        echo "deploy_key_path=\"$deploy_key_path\""
        echo "data_prefix=\"$data_prefix\""
        echo "scratch_prefix=\"$scratch_prefix\""
        echo "mirror_storage=\"$mirror_storage\""
        echo "cache_storage=\"$cache_storage\""
        echo "misc_storage=\"$misc_storage\""
        echo "minio_access_key=\"$minio_access_key\""
        echo "minio_secret_key=\"$minio_secret_key\""
        echo -n "trigger_from_upstream_spack_changes="
        echo "\"$trigger_from_upstream_spack_changes\""
    ) > "$config_file"

    if [ "$sourced" '!=' '0' ] ; then
        load_config
    fi
}

build_utility_image() {
    local check_file="$local_cache_dir/utility-image-built"
    if [ -f "$check_file" ] ; then
        return
    fi

    local packages=""
    packages="${packages} e2fsprogs"

    local id="$( buildah from --pull "$base_image" | tail -n +2 )"

    buildah run "$id" sh -c "dnf -y update"
    buildah run "$id" sh -c "dnf -y install $packages"
    buildah commit "$id" "$util_tag"

    touch "$check_file"
    echo "$id"
}

ensure_network() {
    podman network create "${network}" &> /dev/null || true
}

start_utility() {
    if container_running "$util_container_name" ; then
        return
    fi

    ensure_container_cleared "$util_container_name"
    ensure_network

    podman run \
        --privileged \
        --detach \
        --interactive \
        --tty \
        --name "$util_container_name" \
        --security-opt label=disable \
        --volume "$data_dir:/data:rw,Z" \
        --volume "$scratch_dir:/scratch:rw,Z" \
        --network "${network}" \
        "$util_tag"
}

restart_utility() {
    stop_utility
    start_utility
}

stop_utility() {
    ensure_container_cleared "$util_container_name" &> /dev/null
}

make_volume() {
    local name="$1" ; shift
    local storage_class="$1" ; shift

    local prefix
    if [ "$storage_class" '=' 'scratch' ] ; then
        prefix="$scratch_dir"
    fi

    if [ "$storage_class" '=' 'data' ] ; then
        prefix="$data_dir"
    fi

    if [ -z "$prefix" ] ; then
        echo "Invalid storage class: \"$storage_class\""
        exit 1
    fi >&2

    local device="$prefix/$name"
    mkdir --mode=700 -p "$device"

    podman volume create --label storageClass="$storage_class" \
                         --opt device="$device" \
                         --opt o=bind \
                         "$namespace-$name"
}

clear_volume() {
    podman run \
        --privileged \
        --rm \
        --interactive \
        --tty \
        --security-opt label=disable \
        --volume "${namespace}-$1:/data:rw,nodev,noexec,Z" \
        --entrypoint find \
            "$base_image" /data -mindepth 1 -depth -delete
}

delete_volume() {
    local name="$1" ; shift
    local volume_name="${namespace}-${name}"
    local device="$(
        podman volume inspect --format '{{.Options.device}}' \
            "${volume_name}" | tail -n +2 )"

    if [ -z "$device" ] ; then
        echo "Podman volume not found: ${volume_name}."
        return 1
    fi >&2

    if [ -d "$device" ] ; then
        clear_volume "$name"
        rmdir "$device"
    fi

    podman volume rm "$volume_name"
}

container_running() {
    podman ps -q -f name="$1" | tail -n +2 | grep -q .
    local result="$?"
    return $result
}

container_exists() {
    podman ps -aq -f name="$1" | tail -n +2 | grep -q .
    local result="$?"
    return $result
}

ensure_container_cleared() {
    if container_exists "$1" ; then
        echo "Found old container, deleting..."
    fi

    if container_running "$1" ; then
        podman kill "$1"
    fi

    if container_exists "$1" ; then
        podman rm --force "$1" 2> /dev/null || true
    fi
}

start_minio() {
    if container_running "$minio_container_name" ; then
        return
    fi

    ensure_container_cleared "$minio_container_name"

    ensure_network

    podman run \
        --privileged \
        --detach \
        --name "$minio_container_name" \
        --security-opt label=disable \
        --volume "$namespace-mirror:/mirror:rw,nodev,noexec,Z" \
        --volume "$namespace-cache:/cache:ro,nodev,exec,Z" \
        --network "${network}" \
        --entrypoint "sh" \
            "minio/minio:RELEASE.2020-04-02T21-34-49Z" \
            /cache/global/minio-entrypoint.sh
    # > /dev/null

    ip="$( podman inspect --type container "$minio_container_name" \
            --format '{{.NetworkSettings.IPAddress}}' | tail -n +2 )"

    podman run \
        --privileged \
        --rm \
        --volume "$namespace-cache:/cache:ro,nodev,exec,Z" \
        --network "${network}" \
        --entrypoint sh \
            "minio/mc" /cache/global/minio-config.sh "$ip" || true
}

stop_minio() {
    ensure_container_cleared "$minio_container_name" &> /dev/null
}

start_git_mirror() {
    if container_running "$git_mirror_container_name" ; then
        return
    fi

    ensure_container_cleared "$git_mirror_container_name"
    ensure_network

    podman run \
        --detach \
        --name "$git_mirror_container_name" \
        --security-opt label=disable \
        --volume "$namespace-cache:/cache:rw,nodev,exec,Z" \
        --network "${network}" \
        --entrypoint bash \
        "$base_image" /cache/global/sync-upstream.sh
}

wait_git_mirror() {
    start_git_mirror

    if [ -f "$local_cache_dir/git-mirror-ready" ] ; then
        return
    fi

    local x='while ! git -C /cache/global/spack branch &> /dev/null ; do'
    x="${x} sleep 5 ; done"
    podman exec "$git_mirror_container_name" sh -c "$x"

    touch "$local_cache_dir/git-mirror-ready"
}

clear_git_mirror_tag() {
    podman run \
        --rm \
        --security-opt label=disable \
        --volume "$namespace-cache:/cache:rw,nodev,exec,Z" \
        --entrypoint git \
        "$base_image" \
            -C /cache/global/spack tag -d viz-sdk-head 2> /dev/null || true
}

prepare_storage() {
    make_volume mirror "$mirror_storage"
    make_volume cache "$cache_storage"
    make_volume misc "$misc_storage"

    # ensure gitlab-runner binary is available
    if [ '!' -f "$glr" ] ; then
        url="https://gitlab-runner-downloads.s3.amazonaws.com/latest"
        url="$url/binaries/gitlab-runner-linux-amd64"

        echo "Downloading gitlab-runner..."
        echo
        curl -L --output "$glr" "$url"
        chmod +x "$glr"
    fi

    ensure_network

    local container="$(
        podman run \
            --privileged \
            --detach \
            --rm \
            --interactive \
            --tty \
            --security-opt label=disable \
            --volume "$namespace-mirror:/mirror:rw,nodev,exec,Z" \
            --volume "$namespace-cache:/cache:rw,nodev,exec,Z" \
            --volume "$namespace-misc:/misc:rw,nodev,exec,Z" \
            --network "${network}" \
            "$base_image" | tail -n +2 )"

    local dirs=""
    dirs="${dirs} /cache/global/spack-src"
    dirs="${dirs} /cache/global/spack-stage"
    dirs="${dirs} /cache/pipeline"
    podman exec "$container" sh -c "mkdir -p $dirs"

    local gcache="$container:/cache/global"
    podman cp "$glr" "$gcache/glr"
    podman cp "$script" "$gcache/rgr.sh"
    podman cp "$config_file" "$gcache/config"
    podman cp "$script_dir/scripts/minio-config.sh" "$gcache"
    podman cp "$script_dir/scripts/minio-entrypoint.sh" "$gcache"
    podman cp "$script_dir/scripts/sync-upstream.sh" "$gcache"
    podman cp "$script_dir/scripts/inject-variables.sh" "$gcache"
    podman cp "$script_dir/scripts/inject-payload.sh" "$gcache"

    local egl_dir="/usr/share/glvnd/egl_vendor.d"
    podman cp "$egl_dir/10_nvidia.json" "$gcache/nvidia-egl.json"

    podman stop "$container"
}

delete_storage() {
    (
        trap true ERR

        set +e
        local result
        local tmp
        delete_volume mirror
        result=$?

        delete_volume cache
        tmp=$?
        if [ "$tmp" '!=' '0' ] ; then result="$tmp" ; fi

        delete_volume misc
        tmp=$?
        if [ "$tmp" '!=' '0' ] ; then result="$tmp" ; fi

        exit "$result"
    )
    return $?
}

stop_git_mirror() {
    ensure_container_cleared "$git_mirror_container_name" &> /dev/null
    rm -f "$local_cache_dir/git-mirror-ready"
}

start_builder() {
    if container_running "$builder_container_name" ; then
        return
    fi

    ensure_container_cleared "$builder_container_name"

    ensure_network

    start_minio
    start_git_mirror

    ip="$( podman inspect --type container "$minio_container_name" \
            --format '{{.NetworkSettings.IPAddress}}' | tail -n +2 )"

    podman run \
        --privileged \
        --detach \
        --interactive \
        --tty \
        --security-opt label=disable \
        --name "$builder_container_name" \
        --network "${network}" \
        --volume "$namespace-mirror:/mirror:rw,nodev,exec,Z" \
        --volume "$namespace-cache:/cache:rw,nodev,exec,Z" \
        --volume "$namespace-misc:/misc:rw,nodev,exec,Z" \
        --volume "/opt/intel:/opt/intel:ro,z" \
        --env NVIDIA_VISIBLE_DEVICES=all \
        --env NVIDIA_DRIVER_CAPABILITIES=all \
        "$builder_tag"

    podman exec "$builder_container_name" sh -c \
        "echo \"$ip $minio_container_name minio mirror\" >> /etc/hosts"

    podman exec "$builder_container_name" \
        ln -s /cache/global/glr /usr/local/bin/gitlab-runner
    podman exec "$builder_container_name" mkdir -p /cache/_builds
    podman exec "$builder_container_name" ln -s /cache/_builds /builds
}

stop_builder() {
    ensure_container_cleared "$builder_container_name" &> /dev/null
}

if [ "$sourced" '=' '0' ] ; then
    mode="$1" ; shift || true

    if [ -z "$mode" ] ; then usage ; fi

    if [ "$mode" '=' 'configure' ] ; then
        echo -n "Setting configuration..."
        load_user_config
        write_config
        echo " done"
    fi

    load_config

    if [ "$mode" '=' 'start-mirror'         ] ; then start_minio          ; fi
    if [ "$mode" '=' 'stop-mirror'          ] ; then stop_minio           ; fi
    if [ "$mode" '=' 'clear-mirror'         ] ; then clear_volume mirror  ; fi
    if [ "$mode" '=' 'start-git-mirror'     ] ; then start_git_mirror     ; fi
    if [ "$mode" '=' 'stop-git-mirror'      ] ; then stop_git_mirror      ; fi
    if [ "$mode" '=' 'clear-git-mirror-tag' ] ; then clear_git_mirror_tag ; fi
    if [ "$mode" '=' 'start-builder'        ] ; then start_builder        ; fi
    if [ "$mode" '=' 'stop-builder'         ] ; then stop_builder         ; fi

    if [ "$mode" '=' 'build-utility'        ] ; then build_utility_image  ; fi
    if [ "$mode" '=' 'prepare-storage'      ] ; then prepare_storage      ; fi
    if [ "$mode" '=' 'delete-storage'       ] ; then delete_storage       ; fi

    if [ "$mode" '=' 'shutdown' ] ; then
        do_clean_config=0
        do_purge=0
        do_unregister=0

        for arg in "$@" ; do
            if [ "$arg" '=' '--all' ] ; then
                do_clean_config=1
                do_purge=1
                do_unregister=1
            fi

            if [ "$arg" '=' '--config' ] ; then
                do_clean_config=1
            fi

            if [ "$arg" '=' '--unregister' ] ; then
                do_unregister=1
            fi

            if [ "$arg" '=' '--purge' ] ; then
                do_purge=1
            fi
        done

        if [ "$do_unregister" '=' '1' ] ; then
            unregister
        fi

        stop_git_mirror
        stop_builder
        stop_minio

        if [ "$do_purge" '=' '1' ] ; then
            delete_storage
        fi

        if [ "$do_clean_config" '=' '1' ] ; then
            rm -rf "$local_cache_dir"
        fi
    fi

    if [ "$mode" '=' 'build' ] ; then
        packages=""
        packages="${packages} bzip2"
        packages="${packages} curl"
        packages="${packages} file"
        packages="${packages} gcc"
        packages="${packages} gcc-c++"
        packages="${packages} gcc-gfortran"
        packages="${packages} git"
        packages="${packages} gnupg2"
        packages="${packages} hostname"
        packages="${packages} jq"
        packages="${packages} libglvnd"
        packages="${packages} libglvnd-glx"
        packages="${packages} libglvnd-egl"
        packages="${packages} libglvnd-opengl"
        packages="${packages} libglvnd-devel"
        packages="${packages} make"
        packages="${packages} openssh-clients"
        packages="${packages} patch"
        packages="${packages} python36"
        packages="${packages} python3-pip"
        packages="${packages} sed"
        packages="${packages} unzip"
        packages="${packages} which"

        id="$( buildah from --pull "$base_image" | tail -n +2 )"

        buildah run "$id" sh -c "dnf -y update"
        buildah run "$id" sh -c "dnf -y install $packages"
        buildah run "$id" sh -c "pip3 install --user boto3 yq"
        buildah commit "$id" "$builder_tag"
    fi

    if [ "$mode" '=' 'register' ] ; then
        mkdir -p "$ci_builds" "$ci_cache"

        concurrency=8
        echo "concurrent=$concurrency" > "$config_toml"

        exec "$glr" register                                                 \
          --config "$config_toml" --non-interactive --name "rigel"           \
          --tag-list "$runner_tag_list"                                      \
          --limit "$concurrency" --request-concurrency "$concurrency"        \
          --url "https://$gitlab_host"                                       \
          --registration-token "$runner_registration_token"                  \
          --builds-dir "$ci_builds" --cache-dir "$ci_cache"                  \
          --executor "custom"                                                \
            --custom-config-exec  "$script" --custom-config-args  config-job \
            --custom-prepare-exec "$script" --custom-prepare-args prepare    \
            --custom-run-exec     "$script" --custom-run-args     run        \
            --custom-cleanup-exec "$script" --custom-cleanup-args cleanup

    elif [ "$mode" '=' 'start' ] ; then
        exec "$glr" run --config "$config_toml"

    elif [ "$mode" '=' 'unregister' ] ; then
        unregister

    elif [ "$mode" '=' 'config-job' ] ; then
        echo "{"
        echo "  \"builds_dir\": \"/builds/$project_slug\","
        echo "  \"cache_dir\": \"/cache/$project_slug\","
        echo "  \"builds_dir_is_shared\": true,"
        echo "  \"driver\": {"
        echo "    \"name\": \"Custom Rigel Executor\","
        echo "    \"version\": \"v0.1.0\""
        echo "  }"
        echo "}"

    elif [ "$mode" '=' 'prepare' ] ; then
       start_builder

    elif [ "$mode" '=' 'run' ] ; then
        wait_git_mirror

        glrs="/gitlab-run-scripts/$job_slug"
        script_path="$glrs/$2"

        podman exec "$builder_container_name" mkdir -p "$glrs"
        podman cp --pause=false \
            "$1" "$builder_container_name:$script_path"
        podman exec "$builder_container_name" \
            /bin/bash "/cache/global/inject-variables.sh" "$script_path"
        podman exec \
            "$builder_container_name" \
            /bin/bash "$script_path"

        if [ "$?" '!=' 0 ] ; then
            exit "$BUILD_FAILURE_EXIT_CODE"
        fi

    elif [ "$mode" '=' 'cleanup' ] ; then
        podman exec "$builder_container_name" \
            rm -rf "/gitlab-run-scripts/$job_slug"
    fi
fi
