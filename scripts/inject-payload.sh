. /cache/global/config

export AWS_ACCESS_KEY_ID="$minio_access_key"
export AWS_SECRET_ACCESS_KEY="$minio_secret_key"
export S3_ENDPOINT_URL="http://mirror"
export SPACK_CDASH_AUTH_TOKEN="$cdash_auth_token"
