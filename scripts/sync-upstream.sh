#! /usr/bin/env bash

set -eo pipefail

log() {
    echo "[$( date --iso-8601=seconds )]" "$@"
}

trap "log Exiting cleanly... ; done=1" SIGINT
trap "log Exiting immediately... ; exit 1" SIGTERM
trap "code=$? ; log Failed with exit code $code... ; exit $code" ERR

   dnf -y update \
&& dnf -y install bzip2 curl file git gnupg2 hostname sed unzip which

cd /cache/global
. config
do_trigger="$trigger_from_upstream_spack_changes"

pipeline_endpoint="https://$gitlab_host/api/v4/projects/$gitlab_project_id"
pipeline_endpoint="${pipeline_endpoint}/trigger/pipeline"

git config --global user.email "maintainers@spack.io"
git config --global user.name "Spack Bot"

done=0
last_iter_was_noop=0

while [ "$done" '=' '0' ] ; do
    if [ '!' -d spack ] ; then
        git clone --mirror \
                  --no-tags \
                  --branch develop \
                  "${upstream_spack_repo}" ./spack &> /dev/null
    else
        git -C ./spack fetch origin develop &> /dev/null
    fi

    current_head="$(
        ( git -C ./spack rev-list -n 1 viz-sdk-head || true ) 2> /dev/null )"

    do_update=0

    if [ -z "$current_head" ] ; then
        log "Scheduling build for the first time:"
        echo "    current version: @$( git -C ./spack rev-list -n 1 develop )"
        do_update=1
    else
        latest_head="$( git -C ./spack rev-parse develop )"
        if [ "$current_head" '!=' "$latest_head" ] ; then
            log "Scheduling build for upstream update:"
            echo "    last update   : @$current_head"
            echo "    latest version: @$latest_head"
            do_update=1
        fi
    fi

    if [ "$do_update" '!=' '0' ] ; then
        last_iter_was_noop=0

        if [ -n "$do_trigger" -a "$do_trigger" '!=' '0' ] ; then
            curl -X POST \
                 -F token="$trigger_token" \
                 -F ref="$trigger_ref" \
                 "$pipeline_endpoint" 2> /dev/null | sed 's/^/        /g'
            echo
        fi

        git -C ./spack tag -a -f -m 'viz-sdk-head' viz-sdk-head develop
    elif [ "$last_iter_was_noop" '==' '0' ] ; then
        log "No updates detected.  Waiting for new updates..."
        last_iter_was_noop=1
    fi

    sleep 5
done
