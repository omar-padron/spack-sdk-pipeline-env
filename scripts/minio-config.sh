#! /usr/bin/env sh

ip="$1" ; shift

try_config() {
    mc config host add minio "http://$ip" \
        "$minio_access_key" \
        "$minio_secret_key" 2> /dev/null
    return $?
}

. /cache/global/config
counter=0

until try_config ; do
    sleep 1
    counter="$(( counter + 1 ))"

    if [ "$counter" -ge 10 ] ; then break ; fi
done

if [ "$counter" -ge 10 ] ; then
    try_config  # try one last time
    if [ "$?" '!=' 0 ] ; then
        echo 'Config failed after 10 attempts'
        exit 1
    fi >&2
fi

mc mb minio/mirror 2> /dev/null
