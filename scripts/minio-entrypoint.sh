#! /usr/bin/env sh

. /cache/global/config

export MINIO_ACCESS_KEY="$minio_access_key"
export MINIO_SECRET_KEY="$minio_secret_key"
exec sh /usr/bin/docker-entrypoint.sh minio \
   server --address "0.0.0.0:80" /mirror
