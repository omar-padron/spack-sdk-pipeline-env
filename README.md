# spack-sdk-pipeline-env

Spack environment and scripts for running SDK pipelines on podman containers.

## Instructions

1. Generate Signing Key/Generate Deploy Key/Copy Runner Registration Token
   (`$ ./generate-keys.sh`)
    1. Browse to Gitlab project -> CI/CD settings -> Deploy Keys.
    1. Create new `Deploy Key`. Paste contents of `keys/deploy` under "Key",
       give the key a descriptive title, and enable "Write access allowed".
    1. Browse to Gitlab project -> CI/CD settings -> Runners.
    1. Under "Specific Runners", copy the registration token.
    1. Browse to Gitlab project -> CI/CD settings -> Variables.
    1. Define the following project CI/CD varialbes:
        - `AWS_ACCESS_KEY_ID`: any value at least 8 characters in length
        - `AWS_SECRET_ACCESS_KEY`: any value at least 8 characters in length
        - `DOWNSTREAM_CI_REPO`: `ssh://git@<gitlab-host>/<project-path>.git`
          (replace `gitlab-host` and `project-path`).
        - `S3_ENDPOINT_URL`: `http://sdk-builder-minio` (change if using a
          different S3 endpoint).
        - `SPACK_CDASH_AUTH_TOKEN`: Authorization token from CDash
        - `SPACK_REPO`: `https://github.com/spack/spack.git`
        - `SPACK_REF`: `develop`
        - `SPACK_SIGNING_KEY`: Contents of `keys/package-signing`
1. Edit config file (`$ $EDITOR config`)
    1. Set `runner_registration_token` to the registration token copied above.
    1. Set `deploy_key_path` to the absolute path of `keys/deploy-secret`.
    1. Set `minio_access_key` to the same value as `AWS_ACCESS_KEY_ID` set
       above.
    1. Set `minio_secret_key` to the same value as `AWS_SECRET_ACCESS_KEY` set
       above.
1. Apply configurations (run once, or whenever the above config file changes).
`$ ./run-gitlab-runner.sh configure`
1. Build the runner container (run once, or after (re)configuring).
`$ ./run-gitlab-runner.sh build`
1. Register the runner (run once, or after unregistering an already-registered
   runner).
`$ ./run-gitlab-runner.sh register`
1. Start the local S3 mirror (optional, it will be automatically started if it
   hasn't already been started when a job runs).
`$ ./run-gitlab-runner.sh start-mirror`
1. Start the runner (press ctrl-c to stop the runner).
`$ ./run-gitlab-runner.sh start`
1. To unregister the runner: `$ ./run-gitlab-runner.sh unregister`.
1. To stop the local S3 mirror: `$ ./run-gitlab-runner.sh stop-mirror`.
